// Function to add a product to the cart
function addToCart(productName, price) {
    // Retrieve existing cart items from localStorage or initialize an empty array
    let cartItems = JSON.parse(localStorage.getItem('cart')) || [];

    // Create a new cart item object
    let newItem = {
        name: productName,
        price: price
    };

    // Add the new item to the cart
    cartItems.push(newItem);

    // Update the cart in localStorage
    localStorage.setItem('cart', JSON.stringify(cartItems));

    // Update the cart count display
    updateCartCount(cartItems.length);
}

// Function to update the cart count display
function updateCartCount(count) {
    document.getElementById('cart-count').innerText = count;
}

// Initialization: Set cart count on page load
document.addEventListener('DOMContentLoaded', function() {
    // Retrieve cart items from localStorage
    let cartItems = JSON.parse(localStorage.getItem('cart')) || [];

    // Update the cart count display with the number of items in the cart
    updateCartCount(cartItems.length);
});
