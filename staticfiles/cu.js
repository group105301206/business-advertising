
let cartCount = 0;

function addToCart(productName, price) {
    // Perform the logic to add the product to the cart
    cartCount++;

    // Update the cart count display
    const cartCountElement = document.getElementById('cart-count');
    if (cartCountElement) {
        cartCountElement.textContent = cartCount;
    }

    // Optionally, you can display a confirmation message or perform other actions here
    console.log(`Added ${productName} to cart! Total price: $${price}.`);
}


