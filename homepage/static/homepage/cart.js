function updateCart() {
  let cartItems = JSON.parse(localStorage.getItem('cart')) || [];
  let quantityInputs = document.querySelectorAll('.quantity-input');

  quantityInputs.forEach(function(input, index) {
      cartItems[index].quantity = parseInt(input.value) || 1;
  });

  localStorage.setItem('cart', JSON.stringify(cartItems));
  renderCartItems();
}

function renderCartItems() {
  let cartItems = JSON.parse(localStorage.getItem('cart')) || [];
  let cartContainer = document.getElementById('cart-items');
  cartContainer.innerHTML = '';

  let table = document.createElement('table');
  table.className = 'table';

  let headerRow = table.createTHead().insertRow();
  let headers = ['Number', 'Product Name', 'Product Price', 'Quantity', 'Action']; // Add 'Action' header
  headers.forEach(function(headerText) {
      let headerCell = document.createElement('th');
      headerCell.textContent = headerText;
      headerRow.appendChild(headerCell);
  });

  cartItems.forEach(function(item, index) {
      let row = table.insertRow();

      let cell1 = row.insertCell(0);
      cell1.textContent = index + 1;

      let cell2 = row.insertCell(1);
      cell2.textContent = item.name;

      let cell3 = row.insertCell(2);
      cell3.textContent = 'UGX' + Number.parseFloat(item.price).toFixed(2);

      let cell4 = row.insertCell(3);
      let quantityInput = document.createElement('input');
      quantityInput.type = 'number';
      quantityInput.value = item.quantity || 1;
      quantityInput.min = 1;
      quantityInput.className = 'quantity-input';
      cell4.appendChild(quantityInput);

      let cell5 = row.insertCell(4);
      let deleteButton = document.createElement('button');
      deleteButton.textContent = 'Delete';
      deleteButton.className = 'btn btn-danger';
      deleteButton.onclick = function() {
          // Remove the item from cartItems array
          cartItems.splice(index, 1);
          // Update localStorage
          localStorage.setItem('cart', JSON.stringify(cartItems));
          // Re-render cart items
          renderCartItems();
      };
      cell5.appendChild(deleteButton);
  });

  cartContainer.appendChild(table);
  updateTotalAmount();
}

function updateTotalAmount() {
  let cartItems = JSON.parse(localStorage.getItem('cart')) || [];
  let totalAmount = cartItems.reduce(function(total, item) {
      return total + (item.price * item.quantity);
  }, 0);

  let totalAmountElement = document.getElementById('total-amount');
  if (totalAmountElement) {
      totalAmountElement.textContent = totalAmount.toFixed(2);
  }
}
