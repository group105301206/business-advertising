from django.urls import path
from . import views
from .views import logout_customer


urlpatterns = [
    path('',views.homepage, name="homepage"),
    path('addproduct',views.addproduct,name="addproduct"),
    path('index', views.index, name='index'),
    path('delete/<str:pk>/',views.delete_item, name='delete_item'),
    path('cart',views.Cart,name="cart"),
    path('order_tracking',views.order_tracking,name="order_tracking"),
    path('checkout',views.Checkout,name="checkout"),
    path('about_us',views.about_us,name="about_us"),
    path('customer_homepage',views.customer_homepage,name="customer_homepage"),
    path('search/',views.search_product, name="search_product"),
    path('category/<str:foo>/',views.categories,name="category"),
    path('logout_customer/', logout_customer, name='logout_customer'),
    path('FAQ/', views.FAQ, name='FAQ'),
]
