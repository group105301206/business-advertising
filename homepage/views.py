from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from .models import Add_Product,Category
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from login.views import home


# Create your views here.
@login_required
def homepage(request):
    user_products = Add_Product.objects.filter(user=request.user)
    return render(request,"homepage/index.html",{
      "products": user_products  
    })

def addproduct(request):
    if request.method =="POST":
        category = request.POST['category']
        category_instance = Category.objects.get(name=category)
        productImage = request.FILES['productImage']
        productName = request.POST['productName']
        price = request.POST['price']
        discount= request.POST['discount']
        description = request.POST['description']

        new_product = Add_Product.objects.create(
            user=request.user,
            category=category_instance,
            productImage = productImage,
            productName=productName,
            price=price,
            discount=discount,
            description=description
            
        )
        new_product.save()
        
        # Redirect to a success page or wherever you need
        #return HttpResponseRedirect('homepage/index.html')
        return redirect('homepage')

    return render(request,"homepage/add-product.html",{})
def index(request):
    return render(request, 'index.html')

def delete_item(request,pk):
    item = Add_Product.objects.get(id=pk)
    item.delete()
    return redirect('homepage')

def customer_homepage(request):
    products = Add_Product.objects.all()
    return render(request,'homepage/customer_homepage.html',{
       "products": products 
    })

def search_product(request):
    query = request.GET['searched']

    products = Add_Product.objects.filter(productName__icontains = query)
    context = {
        "products":products,
        "query" : query
    }
    return render(request,'homepage/searched.html',context)

def Cart(request):
    return render(request,'homepage/cart.html')

def Checkout(request):
    return render(request,'homepage/checkout.html')

def order_tracking (request):
    return render(request,'homepage/order_tracking.html')
def about_us (request):
    return render (request,'homepage/about_us.html')

def categories(request, foo):
    #replace hyphens with spaces
    #foo = foo.replace('-',' ')
    #Grab the category from the url
    print(foo)
    #try:
        #look up the category
    selected_category = Category.objects.get(name=foo)
    product = Add_Product.objects.filter(category = selected_category )
    print("i am dope")
    return render(request,"homepage/category.html",{'product':product,'category':selected_category})
        
   # except:
        #messages.success(request,("That category doesnt Exist"))
        #return redirect("customer_homepage")

def logout_customer(request):
    return render(request, 'login/index.html')


# FAQ view
def FAQ(request):
    return render(request, 'homepage/FAQ.html')