document.addEventListener('DOMContentLoaded', function() {
  updateOrderSummary();
});

function updateOrderSummary() {
  let cartItems = JSON.parse(localStorage.getItem('cart')) || [];

  // Calculate total number of items and total price
  let totalItems = 0;
  let totalPrice = 0;

  cartItems.forEach(function(item) {
      totalItems += item.quantity;
      totalPrice += item.price * item.quantity;
  });

  // Update order summary table cells
  document.getElementById('total-items').textContent = totalItems;
  document.getElementById('total-price').textContent = '$' + totalPrice.toFixed(2);

  // Calculate total amount to pay (total price + shipping)
  let shippingCost = 5.00;
  let totalAmount = totalPrice + shippingCost;
  document.getElementById('total-amount').textContent = '$' + totalAmount.toFixed(2);
}



