from django.test import TestCase
from django.urls import reverse

# Create your tests here.


from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from .models import business
from django.http import HttpResponseRedirect


class YourAppViewsTestCase(TestCase):
    
    def setUp(self):
        self.user_data = {
            'username': 'testuser',
            'password': 'testpassword',
            'email': 'test@example.com',
            'first_name': 'Test',
            'last_name': 'User'
        }
        self.user = User.objects.create_user(**self.user_data)
        self.business_data = {
            'TIN': '123456789',
            'type_business': 'Test Business Type'
        }
        self.client.login(username=self.user_data['username'], password=self.user_data['password'])

    def test_home_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signin.html')

    def test_signup_view_get(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signup.html')

    def test_signup_view_post(self):
        response = self.client.post(reverse('signup'), {
            'username': 'newuser',
            'fname': 'New',
            'lname': 'User',
            'email': 'newuser@example.com',
            'TIN': '987654321',
            'type_business': 'New Business Type',
            'pass1': 'newpassword',
            'pass2': 'newpassword'
        })
        self.assertEqual(response.status_code, 302)  # Redirects to signin upon successful signup

    def test_signin_view_get(self):
        response = self.client.get(reverse('signin'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signin.html')

    def test_signin_view_post(self):
        response = self.client.post(reverse('signin'), {
            'username': self.user_data['username'],
            'pass1': self.user_data['password']
        })
        self.assertEqual(response.status_code, 302)  # Redirects to homepage upon successful signin

    def test_signout_view(self):
        response = self.client.get(reverse('signout'))
        self.assertEqual(response.status_code, 302)  # Redirects to home upon signout

    

    def test_business_login_view(self):
        response = self.client.get(reverse('business_login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signin.html')  # Assuming it renders the same template as signin

    def tearDown(self):
        self.client.logout()

# test case for the customer views
class CustomerViewsTestCase(TestCase):
    def test_activate_customer_view(self):
        # Test the activate_customer view
        user = User.objects.get(username='testcustomer')
        token = generate_token.make_token(user)
        response = self.client.get(reverse('activate_customer', args=[urlsafe_base64_encode(force_bytes(user.pk)), token]))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('customer_homepage'))
    # customer signin view test function 
    def test_signin_customer_view(self):
        # Test the signin_customer view
        response = self.client.get(reverse('signin_customer'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signin_customer.html')

        # Test the signin_customer view with POST data
        response = self.client.post(reverse('signin_customer'), {
            'username': 'testcustomer',
            'pass1': 'testpassword2',
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('customer_homepage'))

    # test case function for the customer signup view
    def test_signup_customer_view(self):
        # Test the signup_customer view
        response = self.client.get(reverse('signup_customer'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login/signup_customer.html')

        # Test the signup_customer view with POST data
        response = self.client.post(reverse('signup_customer'), {
            'username': 'testcustomer',
            'firstname': 'Test',
            'lastname': 'Customer',
            'telephone': '123456789',
            'email': 'testcustomer@example.com',
            'pass1': 'testpassword2',
            'pass2': 'testpassword2',
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('signin_customer'))


test_case.test_signup_customer_view()
test_case.test_signin_customer_view()
test_case.test_activate_user_view()
test_case.test_activate_customer

# businness models tests 

class BusinessCustomerModelsTestCase(TestCase):
    def setUp(self):
        # Create some test data
        user = User.objects.create_user(username='testuser', email='testuser@example.com', password='testpassword')
        business.objects.create(user=user, type_business='Test Business', TIN='123456789')
        customer.objects.create(customer=user, telephone='123456789')


    # test case function for the business model 
    def test_business_model(self):
        # Test the business model
        business_obj = business.objects.get(user__username='testuser')
        self.assertEqual(business_obj.type_business, 'Test Business')
        self.assertEqual(business_obj.TIN, '123456789')

    # customer models test 
    def test_customer_model(self):
        # Test the customer model
        customer_obj = customer.objects.get(customer__username='testuser')
        self.assertEqual(customer_obj.telephone, '123456789')

# Run the test case
test_case = BusinessCustomerModelsTestCase()
test_case.setUp()
test_case.test_business_model()
test_case.test_customer_model()
