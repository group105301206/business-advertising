#from django.shortcuts import render
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from E_commerce import settings
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes , force_str
from .tokens import generate_token
from django.core.mail import send_mail, EmailMessage
from .models import business,customer
from django.contrib.auth.hashers import make_password
# Create your views here.
def home(request):
    
    return render(request,'login/index.html')

def signup(request):
    if request.method == "POST":
        username = request.POST["username"]
        print(request.POST)
        fname = request.POST["fname"]
        lname = request.POST['lname']
        email = request.POST['email']
        
        TIN = request.POST['TIN']
        type_business = request.POST['type_business']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        if User.objects.filter(username = username):
            messages.error(request,"Username already exist! Please try some other username")
            return redirect('home')
        
        if User.objects.filter(email=email):
            messages.error(request,"Email already registered")
            return redirect('home')
        
        if len(username)>13:
            messages.error(request,"Usernmae must be under 10 characters")

        if pass1 != pass2:
            messages.error(request,"Passwords didnt match!")

        if not username.isalnum():
            messages.error(request,"Username must be Alpha-Numeric!")
            return redirect("home")
        
        myuser = User.objects.create_user(username,email,pass1)
        myuser.first_name = fname
        myuser.last_name = lname
        myuser.is_active = False

        myuser.save()

        details = business(
        user=myuser,
        TIN = TIN,
        type_business = type_business
        )
        details.save()



        messages.success(request,"Your account has been successfull created.. We have sent you a confirmation email , please confirm your email in order to activate your email.")

        #welcome Email
        subject = "Welcome to Shop-Com , Shop-Com Login ! "
        message = "Hello" + myuser.first_name + "!! \n" +"Welcome to Shop-Com \n Thanks for visiting our websites..\n We have also sent a confirmation email , please confirm your email address inorder to activate your account. \n\n Thanking you.."
        from_email = settings.EMAIL_HOST_USER
        to_list = [myuser.email]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        # Email Address Confirmation Email

        current_site = get_current_site(request)
        email_subject = "Confirm your email"
        message2 = render_to_string('email_confirmation.html', {
            'name':myuser.first_name,
            'domain': current_site.domain,
            'uid' : urlsafe_base64_encode(force_bytes(myuser.pk)),
            'token':generate_token.make_token(myuser),
        })
        email = EmailMessage(
            subject=email_subject,
            body=message2,
            from_email=settings.EMAIL_HOST_USER,
            to=[myuser.email],
        )
        email.fail_silently  = True
        email.send()
        return redirect('signin')
    return render(request,"login/signup.html")   

def signin(request):

    if request.method == 'POST':
        username = request.POST['username']
        pass1 = request.POST['pass1']

        user = authenticate(username=username,password=pass1)

        if user is not None:
            login(request,user)
            fname = user.first_name
            #return render(request,'login/index.html',{'fname':fname})
            return redirect('homepage')
        else:
            print("Invalid Credentials")
            messages.error(request,"Bad credential")
            return redirect('home')
            
    
    return render(request,"login/signin.html")

def signout(request):
    
    logout(request)
    messages.success(request,"Logged Out Successful")
    return redirect('home')

def signup_customer(request):
    if request.method == "POST":
        username = request.POST["username"]
        firstname = request.POST["firstname"]
        lastname = request.POST['lastname']
        telephone = request.POST['telephone']
        email = request.POST['email']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        if User.objects.filter(username = username):
            messages.error(request,"Username already exist! Please try some other username")
            return redirect('signin_customer')
        
        if User.objects.filter(email=email):
            messages.error(request,"Email already registered")
            return redirect('signin_customer')
        
        if len(username)>13:
            messages.error(request,"Usernmae must be under 10 characters")

        if pass1 != pass2:
            messages.error(request,"Passwords didnt match!")

        if not username.isalnum():
            messages.error(request,"Username must be Alpha-Numeric!")
            return redirect("signin_customer")
        
        
        # create an account
        mycustomer = User.objects.create_user(username,email,pass1)
        
        #mycustomer = customer.objects.create(username=username, email=email ,pass1=pass1)
        mycustomer.first_name = firstname
        mycustomer.last_name = lastname 
        mycustomer.is_active = False

        mycustomer.save()
        
        customer_details = customer(
            customer = mycustomer,
            telephone = telephone
        )
        customer_details.save()

        messages.success(request,"Your account has been successfull created.. We have sent you a confirmation email , please confirm your email in order to activate your email.")

        #welcome Email
        subject = "Welcome to Shop-Com , Shop-Com Login ! "
        message = "Hello" + mycustomer.first_name + "!! \n" +"Welcome to Shop-Com \n Thanks for visiting our websites..\n We have also sent a confirmation email , please confirm your email address inorder to activate your account. \n\n Thanking you.."
        from_email = settings.EMAIL_HOST_USER
        to_list = [mycustomer.email]
        send_mail(subject,message,from_email,to_list,fail_silently=True)
        # Email Address Confirmation Email

        current_site = get_current_site(request)
        email_subject = "Confirm your email"
        message2 = render_to_string('email_confirmation1.html', {
            'name':mycustomer.first_name,
            'domain': current_site.domain,
            'uid' : urlsafe_base64_encode(force_bytes(mycustomer.pk)),
            'token':generate_token.make_token(mycustomer),
        })
        email = EmailMessage(
            subject=email_subject,
            body=message2,
            from_email=settings.EMAIL_HOST_USER,
            to=[mycustomer.email],
        )
        email.fail_silently  = True
        email.send()
        return redirect('signin_customer')
    #return render(request,"login/signup.html") 

    return render(request, 'login/signup_customer.html')
def signin_customer(request):
    if request.method == 'POST':
        username = request.POST['username']
        pass1 = request.POST['pass1']

        customer = authenticate(username=username,password=pass1)

        if customer is not None:
            login(request,customer)
            firstname = customer.first_name
            #return render(request,'login/index.html',{'fname':fname})
            return redirect('customer_homepage')
        else:
            print("Invalid Credentials")
            messages.error(request,"Bad credential")
            return redirect('home')
            
    
    #return render(request,"login/signin.html")

    return render(request, 'login/signin_customer.html')

def activate_user(request, uidb64, token, model_class, redirect_url):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = model_class.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, model_class.DoesNotExist):
        user = None

    if user is not None and generate_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        print("User activated successfully:", user , "token:",token)
        return redirect(redirect_url)
    else:
        print("User failed to be activated", user , "token:",token)
        return render(request, 'activation_failed.html')
    #print(token)

def activate_customer(request, uidb64, token):
    print(token)
    return activate_user(request, uidb64, token, User, 'customer_homepage')
    

def activate(request, uidb64, token):
    return activate_user(request, uidb64, token, User, 'homepage')

    # eanT7Ew=24bR+afm





