from django.contrib import admin
from .models import customer,business

# Register your models here.
admin.site.register(customer)
admin.site.register(business)