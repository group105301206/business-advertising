document.addEventListener("DOMContentLoaded", function() {
  const progressItems = document.querySelectorAll("#progress li");
  const expectedArrival = document.getElementById("expected-arrival");
  
  // Simulate order progress
  setTimeout(() => {
    progressItems[0].classList.add("active");
    expectedArrival.textContent = "Expected Arrival: 2 days";
  }, 2000);

  setTimeout(() => {
    progressItems[1].classList.add("active");
  }, 4000);

  setTimeout(() => {
    progressItems[2].classList.add("active");
  }, 6000);

  setTimeout(() => {
    progressItems[3].classList.add("active");
  }, 8000);
});
