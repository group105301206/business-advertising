from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Add_Product(models.Model):
    user = models.ForeignKey(User,null=True, on_delete=models.CASCADE,default=None)
    category = models.ForeignKey(Category,on_delete=models.CASCADE,default=True,null=False)
    productImage = models.ImageField(upload_to="product_imgs/")
    productName = models.CharField(max_length=20)
    price = models.PositiveIntegerField()
    discount = models.DecimalField(max_digits=5, decimal_places=2)
    description =  models.CharField(max_length=200)
