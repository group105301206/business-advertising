from django.urls import path 
from . import views 

# here are the url patterns
urlpatterns = [
    path('edit_profile', views.edit_profile,  name='edit_profile' ),
]
