from django.test import RequestFactory
from django.urls import resolve
from django.test.utils import resolve_template
from . import views

def test_my_account_view():
    # Create a request object
    request = RequestFactory().get('/my-account/')

    # Call the view function
    response = views.my_account(request)

    # Verify that the response is a 200 OK HTTP status code
    assert response.status_code == 200

    # Verify that the response uses the correct template
    template = resolve_template(response)
    assert template.template.name == 'My_account/account.html'

# Call the test function
test_my_account_view()