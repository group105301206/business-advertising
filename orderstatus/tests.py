from django.test import TestCase
from django.test import TestCase
from django.urls import reverse
from.models import Order, OrderStatus
from.views import order_list, order_detail
# Create your tests here.

# class to define the test functions 
class OrderViewsTestCase(TestCase):
    def setUp(self):
        # Create some test data
        Order.objects.create(id=1, name='Test Order 1')
        Order.objects.create(id=2, name='Test Order 2')
        OrderStatus.objects.create(order_id=1, status='Pending')
        OrderStatus.objects.create(order_id=2, status='Completed')

# order list view test function 
    def test_order_list_view(self):
        # Test the order_list view
        response = self.client.get(reverse('order_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'order_list.html')
        self.assertContains(response, 'Test Order 1')
        self.assertContains(response, 'Test Order 2')

        # order detail view test funvtion
    def test_order_detail_view(self):
        # Test the order_detail view
        response = self.client.get(reverse('order_detail', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'order_detail.html')
        self.assertContains(response, 'Test Order 1')
        self.assertContains(response, 'Pending')
# class of test functions for the models files 
class OrderModelsTestCase(TestCase):
    def setUp(self):
        # Create some test data
        Order.objects.create(id=1, price=10.00, date='2023-01-01', description='Test Order 1')
        Order.objects.create(id=2, price=20.00, date='2023-01-02', description='Test Order 2')
        OrderStatus.objects.create(order_id=1, status='new')
        OrderStatus.objects.create(order_id=1, status='pending')
        OrderStatus.objects.create(order_id=2, status='cleared')
    # test case for the order model 
    def test_order_model(self):
        # Test the Order model
        order = Order.objects.get(id=1)
        self.assertEqual(order.price, 10.00)
        self.assertEqual(order.date, '2023-01-01')
        self.assertEqual(order.description, 'Test Order 1')

    # test case for the order status model 
    def test_orderstatus_model(self):
        # Test the OrderStatus model
        order_status = OrderStatus.objects.get(order_id=1, status='new')
        self.assertEqual(order_status.order.price, 10.00)
        self.assertEqual(order_status.order.date, '2023-01-01')
        self.assertEqual(order_status.order.description, 'Test Order 1')
        self.assertEqual(order_status.status, 'new')

# Run the test case for the views 
test_case = OrderViewsTestCase()
test_case.setUp()
test_case.test_order_list_view()
test_case.test_order_detail_view()

# run the test cases for the models 
test_case = OrderModelsTestCase()
test_case.setUp()
test_case.test_order_model()
test_case.test_orderstatus_model()