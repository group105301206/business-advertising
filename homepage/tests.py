from django.test import TestCase,Client
from django.contrib.auth.models import User
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from .views import homepage, addproduct, index
from .models import Add_Product
from django.core.files.uploadedfile import SimpleUploadedFile


# Create your tests here.

class TestUrls(SimpleTestCase):

    def test_homepage_url_resolves(self):
        url = reverse('homepage')
        self.assertEquals(resolve(url).func, homepage)

    def test_addproduct_url_resolves(self):
        url = reverse('addproduct')
        self.assertEquals(resolve(url).func, addproduct)

    def test_index_url_resolves(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)


class UrlsTestCase(TestCase):
    def test_homepage_url(self):
        # Test the homepage URL
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_addproduct_url(self):
        # Test the addproduct URL
        response = self.client.get('/addproduct/')
        self.assertEqual(response.status_code, 200)
    def test_index_url(self):
        # Test the index URL
        response = self.client.get('/index/')
        self.assertEqual(response.status_code, 200)

    def test_delete_item_url(self):
        # Test the delete_item URL
        response = self.client.get('/delete/1/')
        self.assertEqual(response.status_code, 200)
    
     def test_cart_url(self):
        # Test the Cart URL
        response = self.client.get('/cart/')
        self.assertEqual(response.status_code, 200)
    def test_order_tracking_url(self):
        # Test the order_tracking URL
        response = self.client.get('/order_tracking/')
        self.assertEqual(response.status_code, 200)

    def test_customer_homepage_url(self):
        # Test the customer_homepage URL
        response = self.client.get('/customer_homepage/')
        self.assertEqual(response.status_code, 200)
    
    def test_search_product_url(self):
        # Test the search_product URL
        response = self.client.get('/search/?searched=test')
        self.assertEqual(response.status_code, 200)

    def test_logout_customer_url(self):
        # Test the logout_customer URL
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get('/logout_customer/')
        self.assertEqual(response.status_code, 302)

    def test_faq_url(self):
        # Test the FAQ URL
        response = self.client.get('/FAQ/')
        self.assertEqual(response.status_code, 200)
    
    def test_faq_url(self):
        # Test the FAQ URL
        response = self.client.get('/FAQ/')
        self.assertEqual(response.status_code, 200)

    def test_faq_url(self):
        # Test the FAQ URL
        response = self.client.get('/FAQ/')
        self.assertEqual(response.status_code, 200)


# Run the test case
test_case = UrlsTestCase()
test_case.setUp()
test_case.test_homepage_url()
test_case.test_addproduct_url()
test_case.test_index_url()
test_case.test_delete_item_url()
test_case.test_cart_url()
test_case.test_order_tracking_url()
test_case.test_checkout_url()
test_case.test_about_us_url()
test_case.test_customer_homepage_url()
test_case.test_search_product_url()
test_case.test_categories_url()
test_case.test_logout_customer_url()
test_case.test_faq_url()




class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse('homepage')
        self.addproduct_url = reverse('addproduct')
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.client.login(username='testuser', password='12345')

    def test_homepage_view(self):
        response = self.client.get(self.homepage_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/index.html')

    def test_addproduct_view(self):
        with open('media/bicycle.jpg', 'rb') as img:
            response = self.client.post(self.addproduct_url, {
                'category': 'Test Category',
                'productName': 'Test Product',
                'price': '10',
                'discount': '5',
                'description': 'Test Description',
                'productImage': SimpleUploadedFile('test_image.jpg', img.read(), content_type='image/jpeg')
            }, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('homepage'))
        # Add assertions to check if the product is added to the database
        self.assertTrue(Add_Product.objects.filter(productName='Test Product').exists())

    def test_index_view(self):
        # Test the index view
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
    def test_delete_item_view(self):
        # Test the delete_item view
        response = self.client.get(reverse('delete_item', args=[1]))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('homepage'))

    def test_customer_homepage_view(self):
        # Test the customer_homepage view
        response = self.client.get(reverse('customer_homepage'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/customer_homepage.html')

     def test_search_product_view(self):
        # Test the search_product view
        response = self.client.get(reverse('search_product') + '?searched=test')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/searched.html')

    def test_cart_view(self):
        # Test the Cart view
        response = self.client.get(reverse('Cart'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/cart.html')

    def test_checkout_view(self):
        # Test the Checkout view
        response = self.client.get(reverse('Checkout'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/checkout.html')

    def test_order_tracking_view(self):
        # Test the order_tracking view
        response = self.client.get(reverse('order_tracking'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/order_tracking.html')

    def test_about_us_view(self):
        # Test the about_us view
        response = self.client.get(reverse('about_us'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/about_us.html')


    def test_categories_view(self):
        # Test the categories view
        response = self.client.get(reverse('categories', args=['Test-Category']))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/category.html')

    def test_logout_customer_view(self):
        # Test the logout_customer view
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('logout_customer'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

    def test_faq_view(self):
        # Test the FAQ view
        response = self.client.get(reverse('FAQ'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/FAQ.html')




# Run the test case
test_case = ViewsTestCase()
test_case.setUp()
test_case.test_homepage_view()
test_case.test_addproduct_view()
test_case.test_index_view()
test_case.test_delete_item_view()
test_case.test_customer_homepage_view()
test_case.test_search_product_view()
test_case.test_cart_view()
test_case.test_checkout_view()
test_case.test_order_tracking_view()
test_case.test_about_us_view()
test_case.test_categories_view()
test_case.test_logout_customer_view()
test_case.test_faq_view()
    


class CategoryAddProductModelsTestCase(TestCase):
    def setUp(self):
        # Create some test data
        user = User.objects.create_user(username='testuser', email='testuser@example.com', password='testpassword')
        category_obj = category.objects.create(name='Test Category')
        Add_Product.objects.create(user=user, category=category_obj, productImage='test_image.jpg', productName='Test Product', price=100, discount=10.00, description='Test Description')

    def test_category_model(self):
        # Test the category model
        category_obj = category.objects.get(name='Test Category')
        self.assertEqual(category_obj.name, 'Test Category')

    def test_add_product_model(self):
        # Test the Add_Product model
        add_product_obj = Add_Product.objects.get(productName='Test Product')
        self.assertEqual(add_product_obj.user.username, 'testuser')
        self.assertEqual(add_product_obj.category.name, 'Test Category')
        self.assertEqual(add_product_obj.productImage, 'test_image.jpg')
        self.assertEqual(add_product_obj.productName, 'Test Product')
        self.assertEqual(add_product_obj.price, 100)
        self.assertEqual(add_product_obj.discount, 10.00)
        self.assertEqual(add_product_obj.description, 'Test Description')

# Run the test case
test_case = CategoryAddProductModelsTestCase()
test_case.setUp()
test_case.test_category_model()
test_case.test_add_product_model()