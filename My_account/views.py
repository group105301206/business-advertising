from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from login.models import customer as Customer

# Create your views here.
@login_required
def edit_profile(request):
      customer = Customer.objects.get(customer=request.user)
      if request.method == "POST":
            # Update user's profile
            username = request.POST['username']
            first_name = request.POST['firstname']
            last_name = request.POST['lastname']
            telephone = request.POST['telephone']
            password = request.POST['pass1']
            password2 = request.POST['pass2']

            customer.telephone = telephone
            customer.customer.first_name = first_name
            customer.customer.last_name = last_name
            customer.customer.username = username
            if password and password == password2:
                  customer.customer.set_password(password)
            
            customer.customer.save()
            customer.save()
            print("heloo")

            return render(request, 'My_account/account.html', context={"customer": customer})
            

      else:
            return render(request, 'My_account/account.html', context={"customer": customer})
            #return redirect('edit_profile')



