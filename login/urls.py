#from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    #path('business-login/', views.business_login, name='business_login'),
    path('',views.home,name="home"),
    path('signup',views.signup,name="signup"),
    path('signin',views.signin,name="signin"),
    path('signup_customer',views.signup_customer,name="signup_customer"),
    path('signin_customer',views.signin_customer,name="signin_customer"),
    #path('customer_homepage',views.customer_homepage,name="customer_homepage"),
    path('signout',views.signout,name="signout"),
    #path('cart',views.Cart,name="cart"),
    #path('checkout',views.Checkout,name="checkout"),
    path('activate/<uidb64>/<token>',views.activate,name='activate'),
    path('activate_customer/<uidb64>/<token>',views.activate_customer,name='activate_customer'),
]